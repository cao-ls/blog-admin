import Vue from 'vue'
import VueRouter from 'vue-router'
import { Message } from 'element-ui';
// 进度条
import NProgress from 'nprogress'
import 'nprogress/nprogress.css'

import store from '@/store/index'
import util from '@/libs/util.js'

// 路由数据
import routes from './routes'

// fix vue-router NavigationDuplicated
const VueRouterPush = VueRouter.prototype.push
VueRouter.prototype.push = function push(location) {
  return VueRouterPush.call(this, location).catch(err => err)
}
const VueRouterReplace = VueRouter.prototype.replace
VueRouter.prototype.replace = function replace(location) {
  return VueRouterReplace.call(this, location).catch(err => err)
}

Vue.use(VueRouter)

// 导出路由 在 main.js 里使用
const router = new VueRouter({
  routes
})

/**
 * 路由拦截
 * 权限验证
 */
let Autharr = []  //所有的路由列表
function findAuth(data) {
  data.map(r => {
    Autharr.push(r)
    if (r.children && r.children.length > 0) {
      findAuth(r.children)
    }
  })
  return Autharr
}
router.beforeEach(async (to, from, next) => {
  // 确认已经加载多标签页数据 https://github.com/d2-projects/d2-admin/issues/201
  await store.dispatch('d2admin/page/isLoaded')
  // 确认已经加载组件尺寸设置 https://github.com/d2-projects/d2-admin/issues/198
  await store.dispatch('d2admin/size/isLoaded')
  // 进度条
  NProgress.start()
  // 关闭搜索面板
  store.commit('d2admin/search/set', false)

  if (to.matched.some(r => r.meta.auth)) {
    const token = util.cookies.get('token')
    if (token && token !== 'undefined') {
      // 验证当前路由所有的匹配中是否需要有登录验证的,store state  commit数据不同步,异步
     // Autharr = []
     // let hasAutharr = findAuth(store.state.d2admin.menu.aside)  //优化刷新问题
    //  if (hasAutharr.some(i => i.path == to.path)) {  //证明有权限  后续修改为addRouters动态添加,此版本不能发布，卡顿
        next()
    //  }
      // else {  //没权限进入该路由
      //   Message.error("没有权限进入~")
      //   next(false)
      // }
      NProgress.done()
    } else {
      // 没有登录的时候跳转到登录界面
      // 携带上登陆成功之后需要跳转的页面完整路径
      next({
        name: 'login',
        query: {
          redirect: to.fullPath
        }
      })
      // https://github.com/d2-projects/d2-admin/issues/138
      NProgress.done()
    }
  } else {
    next()
  }


})

router.afterEach(to => {
  // 进度条
  NProgress.done()
  // 多页控制 打开新的页面
  store.dispatch('d2admin/page/open', to)
  // 更改标题
  util.title(to.meta.title)
})

export default router
